<table class="table">
    <thead>
        <tr class="danger">
            <th>{!! Lang::get('catrapackage::provinces.' . $province->province) !!} {!! $province->year !!}</th>
            <th>
                <select id="yearchange" class="form-control" style="max-width: 200px;">
                    <option <?php if ($year == 2023) { echo "selected "; } ?> value=2023>2023</option>
                    <option <?php if ($year == 2022) { echo "selected "; } ?> value=2022>2022</option>
                    <option <?php if ($year == 2021) { echo "selected "; } ?> value=2021>2021</option>
                    <option <?php if ($year == 2020) { echo "selected "; } ?> value=2020>2020</option>
                    <option <?php if ($year == 2019) { echo "selected "; } ?> value=2019>2019</option>
                    <option <?php if ($year == 2018) { echo "selected "; } ?> value=2018>2018</option>
                    <option <?php if ($year == 2017) { echo "selected "; } ?> value=2017>2017</option>
                    <option <?php if ($year == 2016) { echo "selected "; } ?> value=2016>2016</option>
                    <option <?php if ($year == 2015) { echo "selected "; } ?> value=2015>2015</option>
                    <option <?php if ($year == 2014) { echo "selected "; } ?> value=2014>2014</option>
                    <option <?php if ($year == 2013) { echo "selected "; } ?> value=2013>2013</option>
                    <option <?php if ($year == 2012) { echo "selected "; } ?> value=2012>2012</option>
                    <option <?php if ($year == 2011) { echo "selected "; } ?> value=2011>2011</option>
                    <option <?php if ($year == 2010) { echo "selected "; } ?> value=2010>2010</option>
                </select>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2"><img src="/storage/photos/shares/logos/{!! $image !!}" class="img img-responsive"></td>
        </tr>
        <tr>
            <td>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.data') !!}</td>
            <td>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.tonnes') !!}</td>
        </tr>
        <tr>
            <td>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.collected') !!}</td>
            <td>{!! $province->collected !!}</td>
        </tr>
        <tr>
            <td>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.recycled') !!}
                <br><small>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.tire_derived_product') !!}</small></td>
            <td>{!! $province->recycled !!}</td>
        </tr>
        <tr>
            <td>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.energy_recovery') !!}
                <br><small>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.tire_derived_fuel') !!}</small></td>
            <td>{!! $province->energy_recovery !!}</td>
        </tr>
        <tr>
            <td>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.diversion_rate') !!}
                <br><small>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.formula') !!}</small></td>
            <td>{!! $province->diversion_rate !!}</td>
        </tr>

    </tbody>
</table>

<script>
    $("#yearchange").change(function () {
        $.ajax({
            url: '/catra/province',
            type: 'get',
            data: 'province=' + $("#current_province").val() + "&year=" + $("#yearchange").val(),
            dataType: 'html',
            success: function (theresult) {
                $("#region").html(theresult);
                return false;
            },
            error: function () {
                alert('error');
            }
        });
    });
</script>
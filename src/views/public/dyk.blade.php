    <hr>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 text-center larger-text">
            <i class="fa fa-info-circle"></i> <strong>{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.did_you_know') !!}</strong> {!! strip_tags($item_text) !!}
            <br><br>
            <a class="btn btn-primary" href="/catra-operations" style="color: white;">{!! \Illuminate\Support\Facades\Lang::get('catrapackage::common.learn_more') !!}</a>
        </div>
        <div class="col-md-1"></div>
    </div>

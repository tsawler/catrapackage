@extends('public.base-inside')


@section('content')
    @include('public.partials.edit-region')
@stop

@section('bottom-js')
    @include('public.partials.vcms-edit-common-js')
    @include('public.partials.vcms-edit-page-js')
    @include('public.partials.vcms-edit-menu-js')
    @include('public.partials.vcms-edit-gallery-js')
    <script>
        $("#yearpicker").change(function () {
            $(".table").addClass('hidden');
            $('#' + $("#yearpicker").val()).removeClass('hidden');
        });
    </script>
@stop

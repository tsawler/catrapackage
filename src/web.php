<?php
Route::group(['middleware' => ['web']], function () {
    Route::get('/catra/province', 'Tsawler\CATRAPackage\ProvincesController@getProvince');
    Route::get('/catra/national-data', 'Tsawler\CATRAPackage\CatraPageController@showProgramPage');
    Route::get('/catra/provincial-data', 'Tsawler\CATRAPackage\CatraPageController@showProvincialData');
});

Route::group(['middleware' => ['web']], function () {
    Route::group(['middleware' => 'auth.pages'], function () {
        Route::get('/admin/data/provincial-data', 'Tsawler\CATRAPackage\DataController@getProvincialData');
        Route::get('/admin/data/item', 'Tsawler\CATRAPackage\DataController@getItem');
        Route::post('/admin/data/item', 'Tsawler\CATRAPackage\DataController@postItem');
        Route::get('/admin/data/delete', 'Tsawler\CATRAPackage\DataController@deleteItem');
    });
});

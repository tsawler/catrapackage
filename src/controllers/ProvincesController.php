<?php namespace Tsawler\CATRAPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

/**
 * Class ProvincesController
 * @package Tsawler\CATRAPackage
 */
class ProvincesController extends Controller
{

    /**
     * @return mixed
     */
    public function getProvince()
    {
        $province = Input::get('province');
        if (Input::has('year'))
            $year = Input::get('year');
        else
            $year = 2023;

        $result = Province::where('province', '=', $province)->where('year', '=', $year)->first();
        $img = Image::where('province', '=', $province)->first();
        $image = $img->image;

        return View::make('catrapackage::public.province')
            ->with('province', $result)
            ->with('image', $image)
            ->with('year', $year);
    }
}

<?php namespace Tsawler\CATRAPackage;

use App\DbView;
use App\Http\Controllers\Controller;
use App\Localize;
use App\Page;
use App\PageDetail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use OpenGraph;
use SEO;
use SEOMeta;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class CatraPageController
 * @package Tsawler\CATRAPackage
 */
class CatraPageController extends Controller
{

    /**
     * @return mixed
     */
    public function showProgramPage()
    {
        $slug = Request::segment(2);
        $page_title = "Not active";
        $page_content = "Either the page you requested is not active, or it does not exist.";
        $meta = "";
        $meta_keywords = "";
        $active = 1;
        $slider = null;
        $slider_sort = "{}";


        if ((Cache::has('page_' . $slug . '_' . App::getLocale())) && (getenv('APP_DEBUG') == false)) {
            $page = Cache::get('page_' . $slug . '_' . App::getLocale());
        } else {
            $page = Page::where('slug', '=', $slug)->first();
            Cache::forever('page_' . $slug . '_' . App::getLocale(), $page);
        }

        if ($page) {
            $page_title_field = Localize::localize('page_title');
            $page_content_field = Localize::localize('page_content');
            $page_title = $page->$page_title_field;
            $page_content = $page->$page_content_field;
            $slider = $page->slider;
            $active = $page->active;
            $meta = $page->meta;
            $meta_keywords = $page->meta_tags;
        } else {
            App::abort(404);
        }

        if ($active == 0) {
            if ((Auth::check()) && (Auth::user()->access_level == 3)) {
                // do nothing
            } else {
                App::abort(404);
            }
        }

        if ($slider != null) {
            $slider_id = $page->slider->id;
            $slider_sort = "{";
            foreach ($slider->slides as $item) {
                $slider_sort .= '"' . $item->id . '":' . $item->sort_order . ",";
            }
            $slider_sort = rtrim($slider_sort, ",");
            $slider_sort .= "}";
        } else {
            $slider_id = 0;
        }

        $generated_content = DbView::blade($page_content);

        SEO::twitter();
        SEO::opengraph();

        $crawler = new Crawler($generated_content);

        $result = $crawler
            ->filterXpath('//img')
            ->extract(['src']);

        $images = [];
        foreach ($result as $image) {
            $images[] = env('SITE_URL') . $image;
        }
        OpenGraph::addProperty('type', 'website');
        SEO::setDescription($page->meta);
        SEO::setCanonical(URL::current());
        SEO::addImages($images);

        SEOMeta::setDescription($meta);
        $keywords = explode(" ", str_replace(",", "", $meta_keywords));
        SEOMeta::addKeyword($keywords);

        return View::make('catrapackage::public.national-data')
            ->with('page', $page)
            ->with('page_title', $page_title)
            ->with('page_content', $generated_content)
            ->with('slider_id', $slider_id)
            ->with('slider_sort', $slider_sort);
    }


    /**
     * @return mixed
     */
    public function showProvincialData()
    {
        $slug = Request::segment(2);
        $page_title = "Not active";
        $page_content = "Either the page you requested is not active, or it does not exist.";
        $meta = "";
        $meta_keywords = "";
        $active = 1;
        $slider = null;
        $slider_sort = "{}";


        if ((Cache::has('page_' . $slug . '_' . App::getLocale())) && (getenv('APP_DEBUG') == false)) {
            $page = Cache::get('page_' . $slug . '_' . App::getLocale());
        } else {
            $page = Page::where('slug', '=', $slug)->first();
            Cache::forever('page_' . $slug . '_' . App::getLocale(), $page);
        }

        if ($page) {
            $page_title_field = Localize::localize('page_title');
            $page_content_field = Localize::localize('page_content');
            $page_title = $page->$page_title_field;
            $page_content = $page->$page_content_field;
            $slider = $page->slider;
            $active = $page->active;
            $meta = $page->meta;
            $meta_keywords = $page->meta_tags;
        } else {
            App::abort(404);
        }

        if ($slider != null) {
            $slider_id = $page->slider->id;
            $slider_sort = "{";
            foreach ($slider->slides as $item) {
                $slider_sort .= '"' . $item->id . '":' . $item->sort_order . ",";
            }
            $slider_sort = rtrim($slider_sort, ",");
            $slider_sort .= "}";
        } else {
            $slider_id = 0;
        }

        $generated_content = DbView::blade($page_content);

        SEO::twitter();
        SEO::opengraph();

        $crawler = new Crawler($generated_content);

        $result = $crawler
            ->filterXpath('//img')
            ->extract(['src']);

        $images = [];
        foreach ($result as $image) {
            $images[] = env('SITE_URL') . $image;
        }
        OpenGraph::addProperty('type', 'website');
        SEO::setDescription($page->meta);
        SEO::setCanonical(URL::current());
        SEO::addImages($images);

        SEOMeta::setDescription($meta);
        $keywords = explode(" ", str_replace(",", "", $meta_keywords));
        SEOMeta::addKeyword($keywords);

        return View::make('catrapackage::public.provincial-data')
            ->with('page', $page)
            ->with('page_title', $page_title)
            ->with('page_content', $generated_content)
            ->with('slider_id', $slider_id)
            ->with('slider_sort', $slider_sort);
    }

}

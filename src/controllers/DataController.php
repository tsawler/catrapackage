<?php namespace Tsawler\CATRAPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


/**
 * Class DataController
 * @package Tsawler\CATRAPackage
 */
class DataController extends Controller
{

    /**
     * @return mixed
     */
    public function getProvincialData()
    {
        $data = Province::orderBy('province', 'year')->get();

        return View::make('catrapackage::admin.provincial-data-list')
            ->with('data', $data);
    }


    /**
     * @return mixed
     */
    public function getItem()
    {
        $id = Input::get('id');

        $provinces = [
            'AB' => 'AB',
            'BC' => 'BC',
            'MB' => 'MB',
            'NB' => 'NB',
            'NL' => 'NL',
            'NS' => 'NS',
            'NT' => 'NT',
            'NU' => 'NU',
            'ON' => 'ON',
            'PE' => 'PE',
            'QC' => 'QC',
            'Sk' => 'SK',
            'YT' => 'YT',
        ];

        if ($id > 0)
            $province = Province::find($id);
        else
            $province = new Province();

        return View::make('catrapackage::admin.provincial-data-item')
            ->with('data', $province)
            ->with('provinces', $provinces);
    }


    /**
     * @return mixed
     */
    public function postItem()
    {
        $id = Input::get('id');
        if ($id > 0)
            $province = Province::find($id);
        else
            $province = new Province();

        $province->province = Input::get('province');
        $province->year = Input::get('year');
        $province->collected = Input::get('collected');
        $province->recycled = Input::get('recycled');
        $province->energy_recovery = Input::get('energy_recovery');
        $province->diversion_rate = Input::get('diversion_rate');
        $province->five_year_average = Input::get('five_year_average');
        $province->save();

        return Redirect::to('/admin/data/provincial-data');
    }


    /**
     * @return mixed
     */
    public function deleteItem()
    {
        $id = Input::get('id');
        Province::find($id)->delete();

        return Redirect::to('/admin/data/provincial-data');

    }
}

<?php namespace Tsawler\CATRAPackage;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

/**
 * Class DidYouKnowController
 * @package Tsawler\CATRAPackage
 */
class DidYouKnowController extends Controller
{

    public static function getDidYouKnow()
    {
        $item = DidYouKnow::orderByRaw("RAND()")->limit(1)->first();
        $field = "item_text_" . Session::get('lang');

        $item_text = $item->$field;

        return View::make('catrapackage::public.dyk')
            ->with('item_text', $item_text);
    }
}

<?php namespace Tsawler\CATRAPackage;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

/**
 * Class CATRAPackageServiceProvider
 * @package Tsawler\CATRAPackage
 */
class CATRAPackageServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *c
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadViewsFrom(__DIR__ . '/views', 'catrapackage');
        $this->loadTranslationsFrom(__DIR__.'/translations', 'catrapackage');
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

}